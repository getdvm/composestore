## WordPress with MariaDB
WordPress is a free and open source blogging tool and a content management system (CMS) based on PHP and MySQL, which runs on a web hosting service. Features include a plugin architecture and a template system.

## Stack
This stack is a template using the latest WordPress release with RDS (MySQL).

![](diagram.svg)
